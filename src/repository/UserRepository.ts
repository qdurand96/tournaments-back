import { RowDataPacket } from "mysql2";
import { User } from "../entity/User";
import { connection } from "./connection";

export class UserRepository {

    static async add(user:User) {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>('INSERT INTO user (username,email,password) VALUES (?,?,?)', [user.username, user.email, user.password]);
        user.id= rows.insertId;
    };

    static async delete(id:string){
        await connection.execute<RowDataPacket[]>(`UPDATE user SET status = 0 WHERE id = ?`,[id])
    };

    static async update(user:User){
        await connection.execute<RowDataPacket[]>('UPDATE user SET email = ?, username = ?, avatar = ? WHERE id = ?', [user.email, user.username, user.avatar, user.id]);
    };

    static async updatePassword(password:string,userId:string){
        await connection.execute<RowDataPacket[]>('UPDATE user SET password = ? WHERE id = ?', [password, userId]);
    };

    static async findByEmail(email:string):Promise<User> {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>('SELECT * FROM user WHERE email = ?', [email]);
        if(rows.length === 1) {
            return new User(rows[0]['email'], rows[0]['password'], rows[0]['username'], rows[0]['role'], rows[0]['avatar'], rows[0]['team_role'], rows[0]['status'], rows[0]['riot_API_id'], rows[0]['team_id'], rows[0]['id']);
        };
        return null;
    };

    static async findById(id:string):Promise<User> {
        const [rows]:any[] = await connection.execute<RowDataPacket[]>(`SELECT * FROM user WHERE id = ?`, [id]);
        if(rows.length===0){
            return
        }
        return new User(rows[0]['email'], rows[0]['password'], rows[0]['username'], rows[0]['role'], rows[0]['avatar'], rows[0]['team_role'], rows[0]['status'], rows[0]['riot_API_id'], rows[0]['team_id'], rows[0]['id']);
    }

    static async findByUsername(username:string):Promise<User> {
        const [rows]:any = await connection.execute<RowDataPacket[]>('SELECT * FROM user WHERE username=?',[username])
        if(rows.length===0){
            return
        }
        return new User(rows[0]['email'], rows[0]['password'], rows[0]['username'], rows[0]['role'], rows[0]['avatar'], rows[0]['team_role'], rows[0]['status'], rows[0]['riot_API_id'], rows[0]['team_id'], rows[0]['id']);
    }

    static async leaveTeam(id:string) {
        await connection.execute<RowDataPacket[]>('UPDATE user SET team_role = NULL, team_id = NULL WHERE id = ?', [id]);
    }
}