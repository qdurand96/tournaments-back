import { RowDataPacket } from "mysql2";
import { Team } from "../entity/Team";
import { User } from "../entity/User";
import { connection } from "./connection";

export class TeamRepository {

    static async add(team: Team, userId: string):Promise<Team> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>('INSERT INTO team (name,avatar) VALUES (?,?)', [team.name,team.avatar]);
        team.id = rows.insertId;
        await connection.execute('UPDATE user SET team_role = ?, team_id = ? WHERE id = ?', ['admin', team.id, userId]);
        return team
    };

    static async delete(id: string) {
        await connection.execute<RowDataPacket[]>(`DELETE FROM team WHERE id = ?`, [id])
    };

    static async update(team: Team) {
        await connection.execute<RowDataPacket[]>('UPDATE team SET name = ?, avatar = ? WHERE id = ?', [team.name, team.avatar, team.id]);
    };

    static async addWin(teamId: string) {
        await connection.execute<RowDataPacket[]>('UPDATE team SET win = win + 1 WHERE id = ?', [teamId]);
    };

    static async findById(id: string, withPlayers: boolean = false):Promise<Team> {
        if (withPlayers) {
            const [rows]: any[] = await connection.execute<RowDataPacket[]>({ sql: "SELECT * FROM team INNER JOIN user ON team.id = user.team_id WHERE team.id = ?", nestTables: true }, [id])
            if (rows.length === 0) {
                return
            }
            const team = new Team(rows[0].team['name'], rows[0].team['avatar'], rows[0].team['win'], rows[0].team['status'], rows[0].team['id']);
            for (const row of rows) {
                if (row.user.id) {
                    const user = new User(row.user['email'], row.user['password'], row.user['username'], row.user['role'], row.user['avatar'], row.user['team_role'], row.user['status'], row.user['riot_API_id'], row.user['team_id'], row.user['id'])
                    team.players.push(user)
                }
            }
            return team
        }

        const [rows]: any[] = await connection.execute<RowDataPacket[]>(`SELECT * FROM team WHERE id = ?`, [id]);
        return new Team(rows[0]['name'], rows[0]['avatar'], rows[0]['win'], rows[0]['status'], rows[0]['id']);
    }

    static async getLadder():Promise<Team[]> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>('SELECT * FROM team ORDER BY win DESC');
        return rows.map((item: Team) => new Team(item['name'], item['avatar'], item['win'], item['status'], item['id']));
    }

    static async isInvited(teamId: string, userId: string):Promise<boolean> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>(`SELECT * FROM invitation WHERE team_id = ? AND user_id = ?`, [teamId, userId]);
        if (rows.length > 0) {
            return true
        }
    }
    static async getInvitations(userId: string):Promise<Team[]> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>({ sql: 'SELECT * FROM team INNER JOIN invitation ON team.id = invitation.team_id WHERE invitation.user_id = ?', nestTables: true }, [userId]);
        return rows.map((item) => new Team(item.team['name'], item.team['avatar'], item.team['win'], item.team['status'], item.team['id']));
    }

    static async sendInvitation(teamId: string, invitedUserId: string) {
        await connection.execute<RowDataPacket[]>('INSERT INTO invitation (team_id,user_id) VALUES (?,?)', [teamId, invitedUserId]);
    };

    static async acceptInvitation(teamId: string, userId: string) {
        await connection.execute<RowDataPacket[]>('UPDATE user SET team_role = ?, team_id = ? WHERE id = ?', ['member', teamId, userId]);
        await connection.execute<RowDataPacket[]>('DELETE FROM invitation WHERE team_id=? AND user_id=?', [teamId, userId]);
    };

    static async declineInvitation(teamId: string, userId: string) {
        await connection.execute<RowDataPacket[]>('DELETE FROM invitation WHERE team_id=? AND user_id=?', [teamId, userId]);
    };

    static async removeFromTeam(toRemoveId: string) {
        await connection.execute<RowDataPacket[]>('UPDATE user SET team_role = NULL, team_id = NULL WHERE id = ?', [toRemoveId])
    }
}