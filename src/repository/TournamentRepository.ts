import { RowDataPacket } from "mysql2";
import { Team } from "../entity/Team";
import { Tournament } from "../entity/Tournament";
import { connection } from "./connection";

export class TournamentRepository {

    static async add(tournament: Tournament, userId: string): Promise<Tournament> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>('INSERT INTO tournament (name, image, date, price, team_nb, text, riot_API_id, user_id) VALUES (?,?,?,?,?,?,?,?)', [tournament.name, tournament.image, tournament.date, tournament.price, tournament.teamNb, tournament.text, tournament.riotAPIId, userId]);
        tournament.id = rows.insertId;
        return tournament
    };

    static async delete(id: string) {
        await connection.execute<RowDataPacket[]>(`DELETE FROM tournament WHERE id = ?`, [id])
    };

    static async update(tournament: Tournament) {
        await connection.execute<RowDataPacket[]>('UPDATE tournament SET name = ?, image = ?, date = ?, price = ?, team_nb = ?, text = ?, riot_API_id = ? WHERE id = ?', [tournament.name, tournament.image, tournament.date, tournament.price, tournament.teamNb, tournament.text, tournament.riotAPIId, tournament.id]);
    };

    static async end(winnerId: string, tournamentId: string) {
        await connection.execute<RowDataPacket[]>('UPDATE tournament SET winner_id = ?, status = 0,  WHERE id = ?', [winnerId, tournamentId]);
    };

    static async findById(id: string, withTeams: boolean = false): Promise<Tournament> {
        if (withTeams) {
            const [rows]: any[] = await connection.execute<RowDataPacket[]>({ sql: "SELECT * FROM tournament LEFT JOIN registration ON tournament.id = registration.tournament_id LEFT JOIN team ON registration.team_id = team.id WHERE tournament.id = ?", nestTables: true }, [id])
            if (rows.length === 0) {
                return
            }
            const tournament: Tournament = new Tournament(rows[0].tournament['name'], rows[0].tournament['image'], rows[0].tournament['date'], rows[0].tournament['price'], rows[0].tournament['team_nb'], rows[0].tournament['text'], rows[0].tournament['winner_id'], rows[0].tournament['status'], rows[0].tournament['riot_API_id'], rows[0].tournament['user_id'], rows[0].tournament['id']);
            for (const row of rows) {
                if (row.team.id) {
                    const team = new Team(row.team['name'], row.team['avatar'], row.team['win'], row.team['status'], row.team['id']);
                    tournament.teams.push(team)
                }
            }
            return tournament;
        }

        const [rows]: any[] = await connection.execute<RowDataPacket[]>(`SELECT * FROM tournament WHERE id = ?`, [id]);
        if (rows.length === 0) {
            return
        }
        return new Tournament(rows[0]['name'], rows[0]['image'], rows[0]['date'], rows[0]['price'], rows[0]['team_nb'], rows[0]['text'], rows[0]['winner_id'], rows[0]['status'], rows[0]['riot_API_id'], rows[0]['user_id'], rows[0]['id']);
    }

    static async getAllTournaments(): Promise<Tournament[]> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>('SELECT * FROM tournament ORDER BY date DESC')
        if (rows.length === 0) {
            return
        }
        return rows.map((item: Tournament) => new Tournament(item['name'], item['image'], item['date'], item['price'], item['team_nb'], item['text'], item['winner_id'], item['status'], item['riot_API_id'], item['user_id'], item['id']));
    }

    static async getUserTournaments(userId): Promise<Tournament[]> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>('SELECT * FROM tournament WHERE user_id = ? ORDER BY date DESC', [userId])
        if (rows.length === 0) {
            return
        }
        return rows.map((item: Tournament) => new Tournament(item['name'], item['image'], item['date'], item['price'], item['team_nb'], item['text'], item['winner_id'], item['status'], item['riot_API_id'], item['user_id'], item['id']));
    }

    static async getTeamTournaments(teamId): Promise<Tournament[]> {
        const [rows]: any[] = await connection.execute<RowDataPacket[]>('SELECT * FROM tournament INNER JOIN registration ON tournament.id = registration.tournament_id  WHERE team_id = ? ORDER BY date DESC', [teamId])
        if (rows.length === 0) {
            return
        }
        return rows.map((item: Tournament) => new Tournament(item['name'], item['image'], item['date'], item['price'], item['team_nb'], item['text'], item['winner_id'], item['status'], item['riot_API_id'], item['user_id'], item['id']));
    }

    // static async getOneWithTeams(tournamentId: string) {
    //     const [rows]: any[] = await connection.execute<RowDataPacket[]>({ sql: "SELECT * FROM tournament LEFT JOIN registration ON tournament.id = registration.tournament_id LEFT JOIN team ON registration.team_id = team.id WHERE tournament.id = ?", nestTables: true }, [tournamentId])
    //     if (rows.length === 0) {
    //         return
    //     }
    //     const tournament: Tournament = new Tournament(rows[0].tournament['name'], rows[0].tournament['image'], rows[0].tournament['date'], rows[0].tournament['price'], rows[0].tournament['team_nb'], rows[0].tournament['text'], rows[0].tournament['winner_id'], rows[0].tournament['status'], rows[0].tournament['riot_API_id'], rows[0].tournament['user_id'], rows[0].tournament['id']);
    //     for (const row of rows) {
    //         if (row.team.id) {
    //             tournament.teams.push(row.team)
    //         }
    //     }
    //     return tournament;
    // }

    static async joinTournament(teamId: string, tournamentId: string) {
        await connection.execute<RowDataPacket[]>('INSERT INTO registration (team_id, tournament_id) VALUES (?,?)', [teamId, tournamentId])
    }

    static async leaveTournament(teamId: string, tournamentId: string) {
        await connection.execute<RowDataPacket[]>('DELETE FROM registration WHERE team_id = ? AND tournament_id = ?', [teamId, tournamentId])
    }

}