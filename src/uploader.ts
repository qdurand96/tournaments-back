import multer from 'multer';
import path from 'path';
import { randomUUID } from 'crypto';

const storage = multer.diskStorage({
    async destination(req,file,cb) {
        const uploadFolder =  __dirname+'/../public/uploads';
        cb(null,uploadFolder);
    },
    filename(req,file,cb) {
        cb(null, randomUUID()+path.extname(file.originalname))
        
    }
});

export const uploader = multer({
    storage,
    limits:{
        fileSize:2000000
    }});