import { Router } from "express";
import { Team } from "../entity/Team";
import { Tournament } from "../entity/Tournament";
import { TeamRepository } from "../repository/TeamRepository";
import { TournamentRepository } from "../repository/TournamentRepository";
import { uploader } from "../uploader";
import { protect } from "../utils/token";


export const tournamentController:Router = Router();

tournamentController.get('/', async (req,res)=>{
    try {
        const tournaments:Tournament[] = await TournamentRepository.getAllTournaments()

        if(!tournaments) {
            res.status(404).json('No tournaments found')
            return
        }

        res.status(200).json(tournaments)
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

tournamentController.get('/user', protect(), async (req,res)=>{
    try {
        const tournaments:Tournament[] = await TournamentRepository.getUserTournaments(req.user.id)

        if(!tournaments) {
            res.status(404).json('No tournaments found')
            return
        }

        res.status(200).json(tournaments)
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

tournamentController.get('/team', protect(), async (req,res)=>{
    try {
        const tournaments:Tournament[] = await TournamentRepository.getTeamTournaments(req.user.teamId)

        if(!tournaments) {
            res.status(404).json('No tournaments found')
            return
        }

        res.status(200).json(tournaments)
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

tournamentController.get('/:id', async (req,res)=>{
    try {
        const tournament:Tournament = await TournamentRepository.findById(req.params.id,true)

        if(!tournament) {
            res.status(404).json('No tournament found')
            return
        }
        res.status(200).json(tournament)
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

tournamentController.post('/', protect(), uploader.single('image'), async (req,res)=>{
    try {
        const newTournament: Tournament = {} as Tournament
        Object.assign(newTournament, req.body);
        if (req.file) {
            newTournament.image = req.file.filename;
        }

        if(req.file===undefined) {
            newTournament.image=null;
        }

        if(!req.body.price) {
            newTournament.price = 1;
        }
        if(!req.body.riotAPIId) {
            newTournament.riotAPIId = null;
        }
        
        newTournament.teamNb=Number(req.body.teamNb)        
        
        await TournamentRepository.add(newTournament, req.user.id)
        res.status(200).json(newTournament)
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

tournamentController.patch('/:id', protect(),uploader.single('image'), async (req,res)=>{
    try {
        let data:Tournament = await TournamentRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        if (data.userId !== req.user.id) {
            res.send(401).end();
            return
        }
        let updated:Tournament = { ...data, ...req.body };
        if (req.file) {
            updated.image = req.file.filename;
        }
        await TournamentRepository.update(updated);
        res.status(200).json(updated);
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

tournamentController.delete('/:id', protect(), async (req, res) => {
    try {
        let toDelete:Tournament = await TournamentRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404).json('No tournament found');
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'admin') {
            await TournamentRepository.delete(String(toDelete.id));
            res.status(204).end();
            return
        }
        res.status(401).end();
        return
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

tournamentController.patch('/end/:id', protect(), async (req, res) => {
    try {
        let toEnd:Tournament = await TournamentRepository.findById(req.params.id);
        if (!toEnd) {
            res.status(404).json('No tournament found');
            return
        }
        await TournamentRepository.end(req.body.winnerId,req.params.id)
        await TeamRepository.addWin(req.body.winnerId)
        res.status(200).json('Tournament ended')
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

tournamentController.post('/registration/:id', protect(), async (req,res) => {
    try {
        let toJoin:Tournament = await TournamentRepository.findById(req.params.id);
        if (!toJoin) {
            res.status(404).json('No tournament found');
            return
        }
        if(req.user.teamRole!== 'admin') {
            res.status(401).json('User is not team admin')
            return
        }
        const team:Team = await TeamRepository.findById(String(req.user.teamId),true)
        if(team.players.length<5) {
            res.status(400).json(`Team doesn't have enough players`)
            return
        }
        await TournamentRepository.joinTournament(req.user.teamId,req.params.id)
        res.status(200).json('Tournament join')
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

tournamentController.delete('/registration/:id', protect(), async (req,res) => {
    try {
        let toLeave:Tournament = await TournamentRepository.findById(req.params.id);
        if (!toLeave) {
            res.status(404).json('No tournament found');
            return
        }
        if(req.user.teamRole!== 'admin') {
            res.status(401).json('User is not team admin')
            return
        }
        await TournamentRepository.leaveTournament(req.user.teamId,req.params.id)
        res.status(200).json('Tournament left')
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})