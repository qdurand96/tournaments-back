import { Router } from "express";
import { Team } from "../entity/Team";
import { User } from "../entity/User";
import { TeamRepository } from "../repository/TeamRepository";
import { UserRepository } from "../repository/UserRepository";
import { uploader } from "../uploader";
import { protect } from "../utils/token";

export const teamController:Router = Router();

teamController.get('/:id', protect(), async (req, res) => {
    try {
        const team = await TeamRepository.findById(String(req.params.id),true)
        if (!team) {
            res.status(404).json('No team found');
            return
        }
        // if(team.status!=1) {
        //     res.status(403).json('desactivated')
        //     return
        // }
        res.status(200).json(team)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

teamController.get('/', async (req, res) => {
    try {
        const ladder:Team[] = await TeamRepository.getLadder()
        if (!ladder){
            res.status(404).end()
            return
        }
        res.status(200).json(ladder)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

teamController.post('/', protect(),uploader.single('avatar'), async (req,res)=>{
    try {
        if (req.user.teamId){
            res.status(400).json('User aleady have a team')
            return
        }
        const newTeam: Team = {} as Team
        Object.assign(newTeam, req.body);

        if (req.file) {
            newTeam.avatar = req.file.filename;
        }

        if(req.file===undefined) {
            newTeam.avatar=null;
        }
        await TeamRepository.add(newTeam,String(req.user.id))
        
        res.status(200).json(newTeam)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

teamController.patch('/', protect(), uploader.single('avatar'), async (req,res)=>{
    try {
        let data:Team = await TeamRepository.findById(String(req.user.teamId));
        if (!data) {
            res.status(404).end();
            return
        }
        let update:Team = { ...data, ...req.body };
        if (req.file) {
            update.avatar = '/uploads/' + req.file.filename;
        }
        await TeamRepository.update(update)
        res.status(200).json(update)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

teamController.delete('/', protect(), async (req, res) => {
    try {
        const team: Team = await TeamRepository.findById(String(req.user.teamId))
        if (!team) {
            res.status(404).end();
            return
        }

        await TeamRepository.delete(String(team.id))
        res.status(204).end()
        
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

teamController.get('/get/invitation', protect(), async (req,res)=>{
    try {
        const invitations:Team[] = await TeamRepository.getInvitations(String(req.user.id))
        if (invitations.length===0){
            res.json(null)
            return
        }
        
        res.status(200).json(invitations)
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

teamController.post('/send/invitation', protect(), async (req,res)=>{
    try {
        
        const invitedUser:User = await UserRepository.findByUsername(req.body.invitedUser)
        if (!invitedUser) {
            res.status(404).json('User not found');
            return
        }
        if(invitedUser.teamId===req.user.teamId){
            res.status(400).json('User already in team')
            return
        }
        const team:Team = await TeamRepository.findById(String(req.user.teamId),true)
        if(team.players.length>=5){
            res.status(400).json('Team already full')
            return
        }
        if(await TeamRepository.isInvited(String(req.user.teamId),String(invitedUser.id))){
            res.status(400).json('User already invited')
            return
        }
        await TeamRepository.sendInvitation(String(req.user.teamId),String(invitedUser.id))
        res.status(200).json('Invitation sent')
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

teamController.patch('/invitation/:teamId', protect(), async (req,res)=>{
    try {
        await TeamRepository.acceptInvitation(req.params.teamId,String(req.user.id))
        res.status(200).json('Invitation accepted')
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

teamController.delete('/invitation/:teamId', protect(), async (req,res)=>{
    try {
        await TeamRepository.declineInvitation(req.params.teamId,String(req.user.id))
        res.status(200).json('Invitation declined')
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

teamController.patch('/remove/:id', protect(), async(req,res) =>{
    try {
        const toRemove:User = await UserRepository.findById(req.params.id)
        if(!toRemove){
            res.status(404).end()
            return
        }

        if(toRemove.teamId !== req.user.teamId || req.user.teamRole !=='admin') {
            res.status(401).end()
            return
        }

        await TeamRepository.removeFromTeam(String(toRemove.id))
        res.status(200).json('User removed')
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})