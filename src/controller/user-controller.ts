import { Router } from "express";
import { User, userSchema } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import { UserRequest } from "../utils/UserRequest";
import { uploader } from "../uploader";
import { TeamRepository } from "../repository/TeamRepository";

export const userController:Router = Router();

userController.post('/register', async (req, res) => {
    try {
        const {error} = userSchema.validate(req.body, {abortEarly:false});
        
        if(error) {
            res.status(400).json({error: error.details.map(item => item.message)});
            return;
        }

        const newUser: User = {} as User
        Object.assign(newUser, req.body);

        const exists: User = await UserRepository.findByEmail(req.body.email)
        if (exists) {
            res.status(400).json({ error: 'Email already taken' });
            return;
        }

        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.add(newUser);

        res.status(201).json({
            user: newUser,
            token: generateToken({
                email: newUser.email,
                id: newUser.id,
                role: newUser.role
            })
        });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.post('/login', async (req, res) => {
    try {
        const user: User = await UserRepository.findByEmail(req.body.email)

        if (user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if (samePassword) {
                if (user.status === 1) {
                    res.json({
                        user,
                        token: generateToken({
                            email: user.email,
                            id: user.id,
                            role: user.role
                        })
                    });
                    return;
                }
                res.status(401).json({ error: 'Disabled account' });
                return;
            }
        }
        res.status(401).json({ error: 'Wrong email and/or password' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.delete('/:id', protect(), async (req, res) => {
    try {
        let toDelete: User = await UserRepository.findById(req.params.id)
        if (!toDelete) {
            res.status(404);
            return
        }

        if (req.user.id === toDelete.id || req.user.role === 'admin') {
            await UserRepository.delete(req.params.id)
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

userController.get('/', protect(), (req, res) => {
    res.json(req.user);
});

userController.get('/:id', async (req: UserRequest, res) => {
    try {
        let user: User = await UserRepository.findById(req.params.id)
        if (!user) {
            res.status(404).end();
            return
        }
        res.status(200).json(user);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

userController.patch('/', protect(), uploader.single('avatar'), async (req: UserRequest, res) => {
    try {
        const {error} = userSchema.validate(req.body, {abortEarly:false});
        
        if(error) {
            res.status(400).json({error: error.details.map(item => item.message)});
            return;
        }        

        let data: User = await UserRepository.findById(String(req.user.id))
        if (!data) {
            res.status(404).end();
            return
        }

        const samePassword: boolean = await bcrypt.compare(req.body.password, data.password);

        if (!samePassword) {
            res.status(401).end()
            return
        }
        let updated: User = { ...data, ...req.body };

        if (req.file) {
            updated.avatar = req.file.filename;
        }
        
        await UserRepository.update(updated)
        res.status(200).json(updated);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

userController.patch('/password', protect(), async (req, res) => {
    try {
        const user: User = await UserRepository.findById(String(req.user.id))
        if (!user) {
            res.status(404).end();
            return
        }

        const samePassword: boolean = await bcrypt.compare(req.body.oldPassword, user.password);

        if (samePassword) {
            const newPassword: string = await bcrypt.hash(req.body.newPassword, 11);
            await UserRepository.updatePassword(newPassword, String(user.id))
            res.status(200).end()
        }
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

userController.patch('/leave', protect(), async (req, res) => {
    try {
        if (!req.user.teamId) {
            res.status(404).json(`User don't have a team`)
        }
        if (req.user.teamRole === 'admin') {
            await TeamRepository.delete(String(req.user.teamId))
            await UserRepository.leaveTeam(String(req.user.id))
            res.status(200).json('Team left and disbanded')
            return
        }
        await UserRepository.leaveTeam(String(req.user.id))
        res.status(200).json('Team left')
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

//getoneuser