import Joi from "joi";

export class User {
    id?:number;
    email?:string;
    password?:string;
    username?:string;
    role?:string;
    avatar?:string;
    teamRole?:string;
    status?:number;
    riotAPIId?:number;
    teamId?:number;

    constructor(
        email:string,
        password:string,
        username:string,
        role:string,
        avatar:string,
        teamRole:string,
        status:number,
        riotAPIId:number,
        teamId:number,
        id:number
    ){
        this.username=username;
        this.email=email;
        this.password=password;
        this.role=role;
        this.avatar=avatar;
        this.teamRole=teamRole;
        this.status=status;
        this.riotAPIId=riotAPIId;
        this.teamId=teamId;
        this.id=id;
    }

    toJSON() {
        if(!this.avatar) {
            return {
                ...this
            }
        }
        return {
            ...this,
            avatar: process.env.SERVER_URL+'/uploads/'+this.avatar
        }
    }
}

export const userSchema = Joi.object({
    email: Joi.string().email(),
    password: Joi.string().required(),
    username: Joi.string(),
    avatar: Joi.string()
});