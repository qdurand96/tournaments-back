import Joi from "joi";
import { Team } from "./Team";

export class Tournament {
    id?:number;
    name?:string;
    image?:string;
    date?:string;
    price?:number;
    teamNb?:number;
    cashprize?:number;
    text?:string;
    winnerId?:string;
    status?:number;
    riotAPIId?:number;
    userId?:number;
    teams?:Team[]=[];

    constructor(
        name:string,
        image:string,
        date:string,
        price:number,
        teamNb:number,
        text:string,
        winnerId:string,
        status:number,
        riotAPIId:number,
        userId:number,
        id:number
    ){
        this.name=name;
        this.image=image;
        this.date=date;
        this.price=price;
        this.teamNb=teamNb;
        this.cashprize=price*teamNb;
        this.text=text;
        this.winnerId=winnerId;
        this.status=status;
        this.riotAPIId=riotAPIId;
        this.userId=userId;
        this.id=id;
    }

    toJSON() {
        if(!this.image) {
            return {
                ...this
            }
        }
        return {
            ...this,
            image: process.env.SERVER_URL+'/uploads/'+this.image
        }
    }

}

export const tournamentSchema = Joi.object({
    name: Joi.string().min(3).required(),
    image: Joi.string(),
    date: Joi.string().required(),
    price: Joi.number(),
    teamNb: Joi.number().required(),
    text: Joi.string().required()
});