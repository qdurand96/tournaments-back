import Joi from "joi";
import { User } from "./User";

export class Team {
    id?:number;
    name?:string;
    avatar?:string;
    win?:number;
    status?:number;
    players?:User[]=[];

    constructor(name:string,avatar:string,win:number,status:number,id:number){
        this.name=name;
        this.avatar=avatar;
        this.win=win;
        this.status=status;
        this.id=id;
    }

    toJSON() {
        if(!this.avatar) {
            return {
                ...this
            }
        }
        return {
            ...this,
            avatar: process.env.SERVER_URL+'/uploads/'+this.avatar
        }
    }
}

export const teamSchema = Joi.object({
    name: Joi.string().min(3).required(),
    avatar: Joi.string()
});
