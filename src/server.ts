import express from 'express';
import cors from 'cors';
import passport from 'passport';
import { teamController } from './controller/team-controller';
import { tournamentController } from './controller/tournament-controller';
import { userController } from './controller/user-controller';
import { configurePassport } from './utils/token';

configurePassport();

export const server = express();

server.use(passport.initialize());
server.use(express.json())
server.use(cors());
server.use(express.static('public'));

server.use('/api/user', userController);
server.use('/api/team', teamController);
server.use('/api/tournament', tournamentController);
