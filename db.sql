DROP DATABASE IF EXISTS tournaments;
CREATE DATABASE tournaments;
USE tournaments;
DROP TABLE IF EXISTS team;
CREATE TABLE team (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (255) NOT NULL,
    avatar VARCHAR (255),
    win INTEGER DEFAULT 0,
    status INTEGER DEFAULT 1 NOT NULL --0=deleted, 1=active
);
DROP TABLE IF EXISTS user;
CREATE TABLE user (
    id INTEGER AUTO_INCREMENT PRIMARY KEY ,
    email VARCHAR (255) NOT NULL,
    password VARCHAR (255) NOT NULL,
    username VARCHAR(12) NOT NULL,
    role VARCHAR (255) DEFAULT 'user' NOT NULL,
    avatar VARCHAR (255),
    team_role VARCHAR (255),
    status INTEGER DEFAULT 1 NOT NULL, --0=desactivated, 1=active
    riot_API_id INTEGER,
    team_id INTEGER,
    FOREIGN KEY (team_id) REFERENCES team(id) ON DELETE SET NULL
);
DROP TABLE IF EXISTS tournament;
CREATE TABLE tournament (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (255) NOT NULL,
    image VARCHAR (255),
    date DATE NOT NULL,
    price INTEGER NOT NULL,
    team_nb INTEGER NOT NULL,
    cashprize INTEGER DEFAULT 0 NOT NULL,
    text TEXT NOT NULL,
    winner_id VARCHAR(255) DEFAULT NULL,
    status INTEGER DEFAULT 1 NOT NULL, --0=over, 1=to come
    riot_API_id INTEGER,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(id)
);
DROP TABLE IF EXISTS registration;
CREATE TABLE registration (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    team_id INTEGER,
    tournament_id INTEGER,
    CONSTRAINT fk_registration_team_id FOREIGN KEY (team_id) REFERENCES team(id) ON DELETE SET NULL,
    CONSTRAINT fk_registration_tournament_id FOREIGN KEY (tournament_id) REFERENCES tournament(id) ON DELETE SET NULL
);
DROP TABLE IF EXISTS invitation;
CREATE TABLE invitation (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    team_id INTEGER,
    user_id INTEGER,
    CONSTRAINT fk_invitation_team_id FOREIGN KEY (team_id) REFERENCES team(id) ON DELETE SET NULL,
    CONSTRAINT fk_invitation_user_id FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE SET NULL
)